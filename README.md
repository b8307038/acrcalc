This tool calculates the **ACR Ped-Score** based on the changes between t6 and t0 in 6 core variables: **AJC** (active joint counts), **LJC** (limited joint counts), **Phys.VAS** (physician global visual analogue scale), **Par.Px.VAS** (parent global VAS), **CHAQ** (childhood health assessment questionnaire), and **ESR** (erythrocyte sedimentation rate).

Steps implemented are detailed, as follows. 
1. Inputs of the core variables are checked to see if 
    - AJC and LJC are integer between 0 and 71.
    - Phys.VAS and Par.Px.VAS range from 0 to 10 in the n2.n1 format, where n1 for up to one decimal digit. 
    - CHAQ ranges from 0 to 3 in the n1.n3 format, where n3 for up to 3 decimal digits. Valid values are multiples of 0.125.
    - ESR (integer) is bounded between 0 and 200.
    - Any one of these violations meets then **CoreVarChecks** is designated for the ACR group.
2. Improvment percentages are then calculated as $` (t6-t0)/t0 \times 100 `$.
3. Missing (NA) core variables are used as detailed below. 
   - A core variable is NA in both t0 and t6, then this core variable is NA.  
   - When data is available at t0 but NA at t6, it checks if t0 is greater than critical values of core varilabes. If this check stands, 29 (26 for CHAQ) is assigned to improvment percentage for this core variable. Otherwise, this core variable remaines NA. Critical values are 55 (71/1.3) for AJC and LJC, 7.7 (10/1.3) for Phys.VAS and , 2.375 for CHAQ, and 154 (200/1.3) for ESR.  
   - When data is NA at t0 but available at t6, it checks if t6 is equal to 0. If this is true, -31 is assigned to improvment percentage. Otherwise, this core variable is NA.  
4. Improvement percentages are -31 (achieved 31$`\%`$ improvement[^1]) for **COVs** other than ESR if both t0 and t6 are 0.
5. Improvement percentages are assigned -31 (achieved 31$`\%`$ improvement[^1]) for ESR if both t0 and t6 are $`<20`$.
6. ACR group is designated using [calacr_impvperR1 function](https://gitlab.com/b8307038/acrcalc/-/blob/master/foo.R)   
    - **NR** for non-responder
    - **COVS MISSING** for insufficient information
    - **ACR70** for $`\geq 3`$ percentages improved by $`\geq 70\%`$ and $`\leq 1`$ percentages worsened by $`> 30\%`$ 
    - **ACR50** for $`\geq 3`$ percentages improved by $`\geq 50\%`$ and $`\leq 1`$ percentages worsened by $`> 30\%`$ 
    - **ACR30** for $`\geq 3`$ percentages improved by $`\geq 30\%`$ and $`\leq 1`$ percentages worsened by $`> 30\%`$ 
 
**AJC**, **Phys.VAS**, **Par.Px.VAS**, and **normalised ESR(nESR)** are needed to calculate Juvenile Disease Activity Score (**JADAS10**)[^2], and the following rules are applied.
1. if AJC $`>10`$ then AJC $`=10`$
2. if ESR $`<20`$ then nESR $`=0`$; if ESR $`>120`$ then nESR $`=10`$; otherwise nESR = $` (ESR-20)/10 `$
3. if any of these 4 core variables is missing, NA is designated
4. **JADAS10** $`=`$ **AJC** $`+`$ **Phys.VAS** $`+`$ **Par.Px.VAS** $`+`$ **nESR**

**AJC**, **Phys.VAS**, **Par.Px.VAS** are used to calculate clinical Juvenile Disease Activity Score (**cJADAS10**)[^3], and the rules below are applied.
1. if AJC $`>10`$ then AJC $`=10`$
2. if any of these 3 core variables is missing, NA is designated
3. **cJADAS10** $`=`$ **AJC** $`+`$ **Phys.VAS** $`+`$ **Par.Px.VAS**

---
[^1]: **CLUSTER Defining Treatment Response_Arthritis_Version2_Current_.docx (19-May-2021)**
[^2]: https://ped-rheum.biomedcentral.com/articles/10.1186/s12969-016-0085-5/tables/1
[^3]: https://onlinelibrary.wiley.com/doi/10.1002/acr.22393
